
public class Feiertag extends Datum{

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Feiertag(int tag, int monat, int jahr, String name) {
		super(tag, monat, jahr);
		this.name = name;
	}

	@Override
	public String toString() {
		return "Feiertag: " +getTag() + "." + getMonat() + "."
				+ getJahr() + " (" + name + ")";
	}
	
	
	
}
