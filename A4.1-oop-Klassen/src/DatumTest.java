
public class DatumTest {

	public static void main(String[] args) {
		Datum d1 = new Datum();
		Datum d2 = new Datum(1 , 8, 1999);

		System.out.println(d1);
		System.out.println(d2);
		System.out.println(d1.equals(d2));
		
		System.out.println(quartal(d2));
	}

	public static String quartal(Datum d) {
		if (d.getMonat()>1 && d.getMonat()<=3)
			return ("Erste Quartal");
		else {
			if(d.getMonat()>3 && d.getMonat()<=6)
				return("Zweite Quartal");
			else {
				if(d.getMonat()>6 && d.getMonat()<=9)
					return("Dritten Quartal");
				else
					return("Vierte Quartal");
			}
		}
	}
	
}
