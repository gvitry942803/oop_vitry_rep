
public class AlterNegativException extends Exception{
	private int fwert;
	
	public AlterNegativException(String msg, int fwert) {
		super(msg);
		this.fwert = fwert;
	}
	
	public int getFwert() {
		return fwert;
	}
	
}
