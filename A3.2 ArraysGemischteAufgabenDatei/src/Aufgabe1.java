import java.util.Scanner;
public class Aufgabe1 {

	static Scanner myScanner = new Scanner(System.in);
	public static void main(String[] args) {
		double[] temperatur = {  4.0,  3.2,  2.5,  1.9,  1.6,  1.1, 
                0.3,  0.0,  1.0,  4.9,  8.8, 13.5, 
               14.1, 15.3, 16.3, 17.1, 17.6, 18.2, 
               17.2, 15.8, 13.5, 13.2, 13.3, 12.0 };
		
		Wetterstatistik temp = new Wetterstatistik();
		Wetterstatistik2 menu = new Wetterstatistik2();
		
		menu.setMenu();
		char selectMenu = myScanner.next().charAt(0);
		if(selectMenu == 'M')
			System.out.println("Mittelwert " + temp.getMittelwert(temperatur));
		if(selectMenu == 'i')
			System.out.println("Minimum " + temp.getMinimum(temperatur));
		if(selectMenu == 'x')
			System.out.println("Maximum " + temp.getMaximum(temperatur));
		if(selectMenu == 'e')
			System.out.println("Bye");
		else 
			System.out.println("wrong entrance");
	}

	
}
