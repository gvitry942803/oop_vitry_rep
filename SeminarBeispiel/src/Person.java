import java.util.Objects;

public class Person {
	/*if we create setters and getters without init a variable
	 * the result will be null because we have to create the attribut*/
	private int personalnummer;
	private String vorname;
	private	int alter;
	private String nachname;
	
	@Override
	public int hashCode() {
		return Objects.hash(personalnummer);
	}

	public boolean equals(Object obj) {
		// instanceof is a verif of class
		if (obj instanceof Person) {
			Person pt = (Person) obj;
			if(this.personalnummer == pt.getPersonalnummer())
				return true; 
			else 
				return false;
		}
		else 
			return false;
	}
/*	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		return personalnummer == other.personalnummer;
	}
*/
	public Person(int personalnummer, String vorname, int alter, String nachname) {
		super();
		this.personalnummer = personalnummer;
		this.vorname = vorname;
		this.alter = alter;
		this.nachname = nachname;
	}

	public int getPersonalnummer() {
		return personalnummer;
	}

	public void setPersonalnummer(int personalnummer) {
		this.personalnummer = personalnummer;
	}

	public String getNachname() {
		return this.nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public Person() {
		this.vorname = "unknow";
		this.alter = 0;
	}
	/*a full construtor have all attribut in parameter*/
	public Person (String vorname, int alter, String nachname) {
		setVorname(vorname);
		setAlter(alter);
		setNachname(nachname);
	}
	
/* getters and setters use when variables are private*/
	public void setVorname(String vorname) {
		this.vorname = vorname; //represent the actual object
	}

	public String getVorname() {
		return this.vorname;
	}
/* put absolute value int the getters works for both 
 * but this is not a good way to code 
 * */
 
	public int getAlter() {
		return alter;
	}

	public void setAlter(int alter) {
		alter = Math.abs(alter);
		this.alter = alter;
	}

	@Override
	public String toString() {
		return "Person [vorname:" + vorname + ", alter:" + alter + ", nachname:" + nachname + "]";
	}

	
}
