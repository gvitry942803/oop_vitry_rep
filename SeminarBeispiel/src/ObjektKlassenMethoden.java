
public class ObjektKlassenMethoden {

	public ObjektKlassenMethoden() {
		System.out.println("Kontructor wurde aufgerufen");
	}
	
	public void objMethode() {
		System.out.println("objMethode");
	}

	public static void klassenMethode() {
		System.out.println("klassenMethode");
	}
}
