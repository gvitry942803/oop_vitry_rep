package Aufgabe1;

public class Divide {
	private int zahl1;
	private int zahl2;
	
	public Divide(int zahl1, int zahl2) {
		this.setZahl1(zahl1);
		this.setZahl2(zahl2);
	}
	
	public int division() throws ArithmeticException{
		if (this.zahl2!=0)
			return this.zahl1/this.zahl2;
		else
			throw new ArithmeticException("Division durch 0");
	}

	public int getZahl1() {
		return zahl1;
	}

	public void setZahl1(int zahl1) {
		this.zahl1 = zahl1;
	}

	public int getZahl2() {
		return zahl2;
	}

	public void setZahl2(int zahl2) {
		this.zahl2 = zahl2;
	}
}
