import java.util.Scanner;

public class Aufgabe1TeilA {

	static Scanner myScanner = new Scanner(System.in);
	static int anzahl;
	
	public static void main(String[] args) {
		anzahl = liesInt("Choose a number");
		incrementation(anzahl);
	}
	
	public static int liesInt(String chooseNumber) {
		System.out.println(chooseNumber);
		return myScanner.nextInt();
	}
	public static void incrementation(int anzahl) {
		int num = 0;
		while (num < anzahl) {
			num++;
			System.out.println(num);
		}
	}
}
