
public class Bankkonto {

	private String inhaber;
	private int kontonummer;
	private double kontostand;
	
	public String getInhaber() {
		return inhaber;
	}
	public void setInhaber(String inhaber) {
		this.inhaber = inhaber;
	}
	public int getKontonummer() {
		return kontonummer;
	}
	public void setKontonummer(int kontonummer) {
		this.kontonummer = kontonummer;
	}
	public double getKontostand() {
		return kontostand;
	}
	public void setKontostand(double kontostand) {
		this.kontostand = kontostand;
	}
	public Bankkonto(String inhaber, int kontonummer, double kontostand) {
		setInhaber(inhaber);
		setKontonummer(kontonummer);
		setKontostand(kontostand);
	}
	
	@Override
	public String toString() {
		return (inhaber + ", nummer: " + kontonummer + ", kontostand: " + kontostand);
	}
	
	public void einzahlen(double amount) {
		kontostand = kontostand + amount;
		setKontostand(kontostand);
		System.out.println("+"+ amount +", New kontostand:"+ kontostand);
	}
	
	public double auszahlen(double amount) {
		kontostand = kontostand - amount; 
		setKontostand(kontostand);
		return amount;
	}
	
}
