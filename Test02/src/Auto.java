
public class Auto {

		private String kennzeichen;
		private String farbe;
		private double geschwindigkeit;
		private static int anzahlObjekte = 0;
		
		public Auto(String kennzeichen, String farbe, double geschwindigkeit){
			// On ne peut pas utiliser set car ils sont pas d�fini
			this.kennzeichen = kennzeichen;
			this.farbe = farbe;
			this.geschwindigkeit = geschwindigkeit; 
			anzahlObjekte++;
		}

		public Auto(){
			this.kennzeichen = "Unbekannnt";
			this.farbe = "wei ";
			this.geschwindigkeit = 0.0; 
			anzahlObjekte++;
		}

		public void setFarbe(String farbe){
			this.farbe = farbe;
		}
		public String getFarbe(String farbe){
			return farbe;
		}

		public double bremse() {						
			double stillstand = geschwindigkeit;
			geschwindigkeit = 0.0;
			return stillstand;
		}

		@Override
		public boolean equals(Object obj){
			if(obj instanceof Auto){
				Auto a = (Auto) obj;
				return(a.kennzeichen.equals(this.kennzeichen)); // Ici ne pas utiliser == car on traite de string et pas de nombre
			}
			else
				return false;
		}

		public static int gibAnzahlObjekte(){
			return anzahlObjekte; 	
			//l'initialisation static permet de savoir combien de contructeur sont appel�s
		}

		@Override
		public String toString(){
			return("Auto-Informationen:" + "\n\t Kennzeichen: " + kennzeichen +"\n\t Farbe: " + farbe
				+ "\n\t Geschindigkeit: " + geschwindigkeit + " km/h");
		}
	

}
