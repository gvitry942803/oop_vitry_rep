import java.util.Scanner;

public class PCHaendler {
	public static 
		String artikel;
	 	static Scanner myScanner = new Scanner(System.in);
	 	static int anzahl;
	 	static double preis;
	 	static double mwst; 
	 	static double nettogesamtpreis;
	 	static double bruttogesamtpreis;
	 	
	public static void main(String[] args) {
		
		
		artikel = liesString("was mochten Sie bestellen?");

		anzahl = liesInt("geben Sie die Anzahl ein:");
		
		preis = liesDouble("Geben Sie den Nettopreis ein:");
		

		mwst = liesDouble1("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		
		nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);		 
		
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}
	public static String liesString(String eingabe) {
		System.out.println(eingabe);
		return myScanner.next();
	}
	
	public static int liesInt(String anzahl) {
		System.out.println(anzahl);
		return myScanner.nextInt();
	}
	
	public static double liesDouble(String preis) {
		System.out.println(preis);
		return myScanner.nextDouble();
	}
	
	public static double liesDouble1(String mwst) {
		System.out.println(mwst);
		return myScanner.nextDouble();
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return (anzahl * nettopreis);
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		return (nettogesamtpreis * (1 + mwst / 100));
	}
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}
