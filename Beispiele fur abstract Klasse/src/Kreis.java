
public class Kreis extends GeometischesObjekt {
	private double radius;

	public Kreis(String farbe, int xpos, int ypos, double radius) {
		super(farbe, xpos, ypos);
		this.radius = radius;
	}
	
	@Override
	public void zeichne() {
		System.out.println("Kreis zeichen");
	}
	
	@Override
	public String toString() {
		return super.toString() + ", " + radius;
	}
}
