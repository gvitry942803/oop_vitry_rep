
public class PersonTest {

	public static void main(String[] args) {
	   //Person p1 = new Person();
/*print object's ref with the name of the class
 * in Java unsigned doesn't exist
 * */		
//		System.out.println(p1); 
//		System.out.println(p1.toString()); 
		
//		p1.vorname = "Max";
/*		
		Person p2 = new Person();
		p2.setVorname("Anna");
		p2.setAlter(18);
		p2.setNachname("M�ller");
//		Person p3 = new Person("Alex");
		
		Person p3 = new Person("Anna", 18, "M�ller");
		
//		System.out.println(p1.vorname);
		System.out.println(p2.getVorname());
		System.out.println(p2.getAlter());
		System.out.println(p2.getNachname());
		System.out.println(p3.getVorname());
		System.out.println(p3.getAlter());
		System.out.println(p3.getNachname());
		
		System.out.println(p2.toString());
		System.out.println(p3); //This print become toSting by default
/* if with == compare the adress of pointeur
 * same for equals
 * We have to implement the method equals
 * 
		if (p2.equals(p3))
			System.out.println("gleich");
		else 
			System.out.println("ungleich");
		*/
		
		
		Teilnehmer t2 = new Teilnehmer(1000, "Alex", "Mustermann", 18, "xyz");
		System.out.println(t2);
		Referent r1 = new Referent(1001, "Thomas", "Durand", 24, "Thales");
		System.out.println(r1);
	}

}
