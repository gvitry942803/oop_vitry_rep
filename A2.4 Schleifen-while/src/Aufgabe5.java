import java.util.Scanner;

public class Aufgabe5 {

	static Scanner myScanner = new Scanner(System.in);
	static int laufzeit;
	static float kapital;
	static float zinssatz;
	static float ausgezahl;
	
	public static void main(String[] args) {
		laufzeit = liesInt("Laufzeit (in Jahren) des Sparvertrags:");
		kapital = liesFloat1("Wie viel Kapital (in Euro) m�chten Sie anlegen:");
		zinssatz = liesFloat("Zinssatz:");
		
		ausgezahl = ausgezahltes(laufzeit, kapital, zinssatz);
		System.out.println(ausgezahl);
	}
	
	public static int liesInt(String laufzeit) {
		System.out.println(laufzeit);
		return myScanner.nextInt();
	}
	
	public static float liesFloat1(String kapital) {
		System.out.println(kapital);
		return myScanner.nextInt();
	}
	
	public static float liesFloat(String zinssatz) {
		System.out.println(zinssatz);
		return myScanner.nextFloat();
	}
	
	public static float ausgezahltes(int laufzeit, float kapital, float zinssatz) {
		while (laufzeit > 0) {
			kapital = kapital * (1+zinssatz/100);
			laufzeit = laufzeit - 1;
		}
		return kapital;
	}

}
