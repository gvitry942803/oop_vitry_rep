
public class Student {
	private String mNr;

	public String getMNr() {
		return mNr;
	}

	public void setMNr(String mNr) {
		this.mNr = mNr;
	}

	public Student(String mNr) {
		
		setMNr(mNr);
	}
/*
 * Override permet de reconnaitre que la fonction est 
 * est d�ja cens� exist� donc affiche une erreur
 * Utilis� pour les h�ritages
 */
	@Override
	public String toString() {
		return "[Matrikelnummer: " + mNr + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Student) {
			//(Student) c'est lire obj comme s'il c'etait la classe Student
			Student st = (Student) obj;
			//On utilise equals car == n'est pas l'id�al pour les String<
			if (this.mNr.equals(st.getMNr()))
				return true;
			else 
				return false;
		}
		//Deuxieme return obligatoire sinon erreur car la method n'est pas un void
		else
			return false;
	}
	
}
