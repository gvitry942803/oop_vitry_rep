
public class GeometrischesObjektTest {

	public static void main(String[] args) {
		//GeometischesObjekt go = new GeometischesObjekt();

		Kreis k1 = new Kreis("rot", 1, 3, 7.5);
		
		System.out.println(k1);
		
		k1.move(3, -5);
		System.out.println(k1);
		
		System.out.println(k1.berechneFlaeche());
		System.out.println(k1.berechneUmfang());
	}

}
