//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.LinkedList;
//import java.util.List;
import java.util.*;
public class ContainerBeispiele {

	public static void main(String[] args) {
		/* On ecrit String avec un S masjuscule parce que String est une classe */
		 String str = "str1"; 
		 
		 String[] sliste = new String[1];
		 sliste[0] = str;
		 
		 ArrayList<String> sliste2 = new ArrayList<String>();
		 sliste2.add(str);
		 sliste2.add("str2");
		 sliste2.add("str3");
		 sliste2.add("str4");
		 
		 List<String> sliste3 = new ArrayList<String>(10);
		 sliste3.add(str);
		 sliste3.add("str2");
		 
		 /* toString est deja implémenté dans ArrayList */
		 System.out.println(sliste2);
		
		 // sliste2.remove(str);
		 // sliste2.remove(2);
//		 sliste2.add(1, "test");
//		 sliste2.set(1, "Klausur");
//		 sliste2.addAll(sliste3);
//		 System.out.println(sliste2);
//		 sliste2.retainAll(sliste3);
//		 System.out.println(sliste2);
		 
//		 for(int i=0; i < sliste3.size(); i++)
//			 System.out.print(sliste3.get(i)+ " ");
//		 System.out.println();
//		 
//		 for(int i=sliste3.size() -1; i >=0; i--)
//			 System.out.print(sliste3.get(i) + " ");
//		 System.out.println();
//		 
//		 for(String s : sliste3)
//			 System.out.print(s+ " ");
		 
		 Iterator<String> it = sliste2.iterator();
		 while (it.hasNext())
			 System.out.print(it.next() + " ");
		 System.out.println();
		 
		 ListIterator<String> lit = sliste2.listIterator(sliste2.size());
		 while(lit.hasPrevious())
			 System.out.print(lit.previous() + " ");
	}

}
