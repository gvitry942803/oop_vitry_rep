
public class Aufgabe1 {

	public static void main(String[] args) {
		int[] tab = new int[10];
		generateTable(tab);
	}
	
	public static void generateTable(int[] table) {
		System.out.print("[ ");
		for(int i=0; i < table.length; i++) {
			table[i] = i;
			System.out.printf("%d ", table[i]);
		}
		System.out.print("]");
	}

}
