
public class Mittelwert {
	public static
		double x;
		static double y;
		static double m;
   public static void main(String[] args) {

	   eingabenFestlegen(x,y);
     
      m = eingabenVerarbeiten(x,y);
       
      gebenErgebnisseAus(x,y,m);
   }
   
   
   public static double eingabenVerarbeiten(double zahl1, double zahl2) {
	   return ((zahl1 + zahl2) / 2);
   }
   public static void eingabenFestlegen(double zahl1, double zahl2) {
	   zahl1 = 2.0;
	   zahl2 = 4.0;  
   }
   public static void gebenErgebnisseAus(double zahl1, double zahl2, double ergebnisse) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl1, zahl2, ergebnisse);
   }
}
