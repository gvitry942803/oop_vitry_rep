
public class Test {

	public static void main(String[] args) {
		
		VerdeckteElem ve = new VerdeckteElem();
		SubVerdeckteElem se = new SubVerdeckteElem();
		
//		System.out.println(ve.objAttr);
//		System.out.println(ve.klAttr);
		//Avec l'�criture suivante, l'�lement de la classe m�re prend le dessus
//		System.out.println(((VerdeckteElem)se).objAttr);
//		System.out.println(((VerdeckteElem)se).klAttr);
		
		ve.objMethode();
		ve.klMethode();
		
		((VerdeckteElem)se).objMethode(); //Ici on aura VE pas SE
		((VerdeckteElem)se).klMethode(); // Ici on a SE
		// Classe Methode bedeckte , Attribut unbedeckete
	
		
	}
}
