package Aufgabe1;

public class ArithmeticException extends Exception{
	
	public ArithmeticException(String msg) {
		super(msg);
	}
}
