package aufgabe2;

public class DatumTest {

	public static void main(String[] args) {
		
		try {
			Datum d = new Datum(0, 0, 0);
			d.setTag(0);
			d.setMonat(0);
			d.setJahr(0);
		} 
//		catch(DatumException ex) {
//			System.out.println(ex.getMessage());
//			System.out.println(ex.getfTag());
//			System.out.println(ex.getfMonat());
//			System.out.println(ex.getfJahr());
//		}
		catch(TagException ex) {
			System.out.println(ex.getMessage());
			System.out.println(ex.getfTag());
		}
		catch(MonatException ex) {
			System.out.println(ex.getMessage());
			System.out.println(ex.getfMonat());
		}
		catch(JahrException ex) {
			System.out.println(ex.getMessage());
			System.out.println(ex.getfJahr());
		}

	}

}
