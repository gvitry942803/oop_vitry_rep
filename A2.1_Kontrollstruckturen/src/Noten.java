import java.util.Scanner;

public class Noten {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Select Student Note");
		
		int note = myScanner.nextInt();
		
		switch(note) {
		case 1: System.out.printf("%d = Sehr gut\n", note);
		break;
		case 2: System.out.printf("%d = Gut\n", note);
		break;
		case 3: System.out.printf("%d = Befriedigend\n", note);
		break;
		case 4: System.out.printf("%d = Ausreichend\n", note);
		break;
		case 5: System.out.printf("%d = Mangelhaft\n", note);
		break;
		default: System.out.printf("%d = Ungenügend\n", note);
		}
	}

}
