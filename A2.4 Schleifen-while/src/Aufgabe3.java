import java.util.Scanner;

public class Aufgabe3 {

	static int Somme;
	static int anzahl;
	static Scanner myScanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		anzahl = liesInt("Choose a number");
		Somme = Sum(anzahl);
		System.out.println(Somme);
	}

	public static int liesInt(String chooseNumber) {
		System.out.println(chooseNumber);
		return myScanner.nextInt();
	}
	
	public static int Sum(int anzahl) {
		 int sum = 0; 
		 while (anzahl > 0) {
			 sum = sum + anzahl %10;
			 anzahl = anzahl/10;
		 }
		 return sum;
	}

	
}
