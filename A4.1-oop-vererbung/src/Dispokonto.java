
public class Dispokonto extends Bankkonto {
	
	private double dispokredit;
	
	public Dispokonto(String inhaber, int kontonummer, double kontostand, double dispokredit) {
		super(inhaber, kontonummer, kontostand);
		this.dispokredit = dispokredit;
	}
	
	public void setDispokredit(double dispokredit) {
		this.dispokredit = dispokredit;
	}
	public double getDispokredit() {
		return dispokredit;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", " + dispokredit;
	}

//	public double auszahlen() {
//		if (getKontostand() < 0)
//			return getDispokredit() - Math.abs(getKontostand());
//		return getDispokredit();
//	}
	
	@Override
	public double auszahlen(double amount) {
		double neuerKontostand = super.getKontostand() - amount;
		if (neuerKontostand >= -1*dispokredit ) {
			setKontostand(neuerKontostand);
			return amount;
		}
		amount = getKontostand() + dispokredit;
		setKontostand(-1 * dispokredit);
		return amount;
	}
}
