
public abstract class GeometischesObjekt {
	protected String farbe;
	protected int xpos;
	protected int ypos;
	public GeometischesObjekt(String farbe, int xpos, int ypos) {
		super();
		this.farbe = farbe;
		this.xpos = xpos;
		this.ypos = ypos;
	}
	
	public abstract void zeichne();
	
	public void move(int x, int y){
		xpos += x;
		ypos += y;
	}
	
	@Override
	public String toString(){
		return farbe + " , ( " + xpos + "," + ypos + ")";
	}

}
