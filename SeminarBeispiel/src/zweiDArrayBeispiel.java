/*
 * Two dimensions Array
 * to find a number by position : array[x][y]
 * Positions not init are 0
 * spalte = colonne
 */
public class zweiDArrayBeispiel {

	public static void main(String[] args) {
		 
		int[][] matrix = new int[][] {
			{1,2,3,4},
			{4,5,6,7},
			{8,9,10,11}
			};
		int[][] matrix2 ={{1,2,3,4},{4,5,6,7},{8,9,10,11}};
		//System.out.println(matrix[1][1]);
		
		printMatrix2(matrix2);
	}
	
	public static void printMatrix(int [][] matrix) {
		
		for(int[] zeile : matrix) {
			for (int elem : zeile) {
				System.out.printf("%-3d", elem);
			}
			System.out.println();
		}
	}
	
public static void printMatrix2(int [][] matrix) {
		
		for(int zeile = 0; zeile < matrix.length; zeile++) {
			for (int spalte = 0; spalte < matrix[zeile].length; spalte++) {
				System.out.printf("%-3d", matrix[zeile][spalte]);
			}
			System.out.println();
		}
	}

}
