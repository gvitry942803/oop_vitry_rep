import java.util.Scanner;

public class Aufgabe2b {
	
	static Scanner myScanner = new Scanner(System.in);
	static int anzahl;
	static int Somme;
	public static void main(String[] args) {
		
		anzahl = liesInt("Choose a number");
		Somme = Sum(anzahl);
		System.out.println(Somme);
	}

	public static int liesInt(String chooseNumber) {
		System.out.println(chooseNumber);
		return myScanner.nextInt();
	}
	
	public static int Sum(int anzahl) {
		int sum = 0;
		for (int i=1; i<anzahl+1; i++) {
			sum = sum + 2*i;
		}
		return(sum);
	}
}
