package aufgabe2;

public class DatumException extends Exception{

	private int ftag;
	private int fmonat;
	private int fjahr;
	
	public DatumException(String msg, int ftag, int fmonat, int fjahr) {
		super(msg);
		
		this.ftag = ftag;
		this.fmonat = fmonat;
		this.fjahr = fjahr;
		
	}
	public int getfTag() {
		return ftag;
	}
	
	public int getfJahr() {
		return fjahr;
	}
	
	public int getfMonat() {
		return fmonat;
	}
	
	
	
	
	
}
