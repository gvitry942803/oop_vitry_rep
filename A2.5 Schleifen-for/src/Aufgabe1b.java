import java.util.Scanner;

public class Aufgabe1b {
	
	static Scanner myScanner = new Scanner(System.in);
	static int anzahl;
	
	public static void main(String[] args) {
		anzahl = liesInt("Choose a number");
		decrementation(anzahl);
	}
	
	public static int liesInt(String chooseNumber) {
		System.out.println(chooseNumber);
		return myScanner.nextInt();
	}
	
	public static void decrementation(int anzahl) {
		for(int i=0; i<anzahl; anzahl--) {
			System.out.println(anzahl);
		}
	}
}
