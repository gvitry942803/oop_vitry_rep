
public class TimeException extends Exception {

	public TimeException(String msg){
		super(msg);
	}

	public TimeException(){
		super("WrongTime");
	}
}