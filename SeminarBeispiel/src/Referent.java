
public class Referent extends Person{

	private String firma;
	
	public Referent() {
		super();
		this.firma = "unbekannt";
	}
	
	public Referent(int personalnummer, String vorname, String nachname,
			int alter, String firma) {
		super(personalnummer, vorname, alter, nachname);
		this.firma = firma;
	}

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}
	
	@Override
	public String toString () {
		return "Person [vorname:" + getVorname() + ", alter:" + getAlter() + ", nachname:" + getNachname() + ", Firma:" + firma + "]";
	}
	
}
