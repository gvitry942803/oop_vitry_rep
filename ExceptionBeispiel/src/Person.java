
public class Person {
	protected int  personalnummer;
	private String vorname;
	private String nachname;
	private int alter;
	
	public Person(){
		this.vorname= "unknown";
		this.alter = 0;
	}
	
	public Person(String vorname, int alter) throws AlterNegativException{
		setVorname(vorname);
		setAlter(alter);
	}
	
	public Person(String vorname, String nachname, int alter) throws AlterNegativException{
		setVorname(vorname);
		setNachname(nachname);		
		setAlter(alter);
	}
	
	
	
	public Person(int personalnummer, String vorname, 
			      String nachname, int alter) {
		this.personalnummer = personalnummer;
		this.vorname = vorname;
		this.nachname = nachname;
		this.alter = alter;
	}

	public int getPersonalnummer() {
		return personalnummer;
	}

	public void setPersonalnummer(int personalnummer) {
		this.personalnummer = personalnummer;
	}

	public void setVorname(String vorname){
		this.vorname = vorname;
	}
	public String getVorname(){
		return this.vorname;
	}
	public void setNachname(String nachname){
		this.nachname = nachname;
	}
	public String getNachname(){
		return this.nachname;
	}
	public void setAlter(int alter) throws AlterNegativException{
		if (alter > 0)
			this.alter = alter;
		else 
			throw new AlterNegativException("Der Wert der Parameter " + "\'Alter\' ist negativ", alter);
	}
	public int getAlter(){
		return this.alter;
	}
	
	public String toString(){
		return "( Nachname: " + this.nachname +
				", Vorname: " + this.vorname +
				", Alter: " + this.alter + ")";
	}
	
	public boolean equals(Object obj){
		
		if (obj instanceof Person){
			Person pt = (Person) obj;
			if (this.personalnummer == pt.getPersonalnummer())
				return true;
			else
				return false;
		}
		else 
			return false;
		
	}

}