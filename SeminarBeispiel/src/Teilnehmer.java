
public class Teilnehmer extends Person{

	/* Un attribut en Protected est priv� mais peut �tre utilis� par toutes les
	 *  classes h�ritages.
	 */
	private String status;
	
	
	public Teilnehmer() {
//		this.personalnummer = 0; 
		//on ne peut pas �crire ca SI l'attribut est private dans Person
		super();
		this.status = "unbekannt";
	}
	
	public Teilnehmer(int personalnummer, String vorname, String nachname, int alter, String status) {
		super(personalnummer, vorname, alter, nachname);
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString () {
		return "Person [vorname:" + getVorname() + ", alter:" + getAlter() + ", nachname:" + getNachname() + ", Status:" + status + "]";
	}
}
