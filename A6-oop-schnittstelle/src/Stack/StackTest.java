package Stack;

public class StackTest {

	public static void main(String[] args) {
		/*
		 * Quand on programme correctement on d�clare la variable 
		 * comme �tant unt variable de l'interface car elle 
		 * comprend toutes les classes qui utilisent l'interface
		 */
		intStack s = new Stack(4);
		
		s.push(1);
		s.push(5);
		s.push(7);
		s.push(9);
		System.out.println(s.pop());

		System.out.println(s);
		
		s.push(13);
		System.out.println(s);

	}

}
