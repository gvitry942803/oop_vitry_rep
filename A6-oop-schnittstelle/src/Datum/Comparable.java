package Datum;

public interface Comparable<T>{

	int compareTo(T obj);
}
