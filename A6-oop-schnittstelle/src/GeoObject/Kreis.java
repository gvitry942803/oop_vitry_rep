package GeoObject;


public class Kreis extends GeoObject{
	private int radius;
	private double PI = 3.14;

	public Kreis(double x, double y, int radius) {
		super(x, y);
		this.radius = radius;
	}

	@Override
	public double determineArea() {
		return PI * radius * radius;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", " + radius;
	}

	
}
