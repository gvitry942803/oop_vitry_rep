
public class Person implements Comparable<Person>{
	
	private int personalnummer;
	private String name;
	

	public Person(int personalnummer, String name) {
		setName(name);
		setPersonalnummer(personalnummer);
	}
	
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getPersonalnummer() {
		return personalnummer;
	}
	public void setPersonalnummer(int personalnummer) {
			this.personalnummer = personalnummer;
	}
	
	public String toString() {   //  [ Name: Max , Alter: 18 ]
		return "[ Name: " + this.name + " , Personalnummer: " + this.personalnummer + " ]";
	}
	
	@Override
	public int compareTo(Person p) {
//		if (personalnummer < p.getPersonalnummer())
//				return -1;
//		if (personalnummer > p.getPersonalnummer())
//			return 1;
//		return 0;
		
		return this.personalnummer - p.getPersonalnummer();
		
	}
	
}