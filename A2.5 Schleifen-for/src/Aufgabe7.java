
public class Aufgabe7 {

	public static void main(String[] args) {
		 int N = 100;
		 prime_N(N);

	}

	static void prime_N(int N)
    {
        int flg;
        for (int i = 1; i <= N; i++) {
            flg = 1;
 
            for (int j = 2; j <= i / 2; ++j) {
                if (i % j == 0) {
                    flg = 0;
                }
            }
 
            if (flg == 1)
                System.out.print(i + " ");
        }
    }
}

