package GeoObject;


public class GeoObjectTest {

	public static void main(String[] args) {
		
		Rechteck r = new Rechteck(3, 4, 1, 2);
		Kreis k = new Kreis(5, 6, 2);
		
		System.out.println(r);
		System.out.println(k);
		
		System.out.println(r.determineArea());
		System.out.println(k.determineArea());
	}
}
