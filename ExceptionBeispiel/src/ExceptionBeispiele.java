
public class ExceptionBeispiele {

	public static void main(String[] args) {
		/*
		 * String[] args sert � pouvoir afficher des exceptions
		 * on peut d�cider grace �  ce parametre les exceptions 
		 * que l'on veut
		 */
		try {
			int zahl = Integer.parseInt(args[0]);
			zahl++;
			System.out.println(zahl);
		}
		catch(ArrayIndexOutOfBoundsException ex) {
			System.out.println("Element nicht vorhanden");
		}
		catch(NumberFormatException ex) {
			System.out.println("Format Error");
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		catch(Exception ex) {
			System.out.println("Error");
		}
	}

}
