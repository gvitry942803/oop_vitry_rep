public class Time implements Comparable<Time>{
	private int hours;
	private int minutes;
	private int seconds;
	
	public static void main(String[] args) {
		try {
			Time t = new Time(2,3,20);
			Time t2 = new Time(2,3,45);
			
			System.out.println(t.compareTo(t2));
		}
		catch(TimeException ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	public Time(int h, int m, int s) throws TimeException {
		if((h>0 && h < 25) && (m>=0 && m<60) && (s>=0 && s<60)) {
			 hours = h;
			 minutes = m;
			 seconds = s;
		}
		else
			throw new TimeException();
//			throw new TimeException("WrongTime");
	}
	
	public Time() {
		 hours = 0;
		 minutes = 0;
		 seconds = 0;
	}
	public int getHours() { 
		 return hours;
	}
	public int getMinutes() { 
		 return minutes;
	}
	public int getSeconds() { 
		 return seconds;
	}
	
	@Override
	public String toString() {
		return hours+":"+minutes+":"+seconds;
	}
	
	@Override
	public int compareTo(Time t) {
		if (hours == t.getHours())
			if (minutes == t.getMinutes())
				return seconds - t.getSeconds();
			else
				return minutes - t.getMinutes();
		else
			return hours - t.getHours();
	}
	
//	@Override
//	public int compareTo(Time t) {
//		return toString().compareTo(t.toString());
//	}
}