
public class TimeExact extends Time{
	int zehntel;
	int hundertstelse;
	
	public TimeExact(int hours, int minutes, int seconds,
			int zehntel, int hundertstelse) throws TimeException {
		super(hours, minutes, seconds);
		this.zehntel = zehntel;
		this.hundertstelse = hundertstelse;
	}
	
	public TimeExact() {
		super();
		this.zehntel = 0;
		this.hundertstelse = 0;
	}
	
	@Override
	public String toString() {
		return getHours()+":"+getMinutes()+":"+getSeconds()+":"+ zehntel+":"+hundertstelse;
//		return super.toString() + ":" + zehntel + ":" + hundertstelse;
	}

	public static void main(String[] args){
		try {
			TimeExact t = new TimeExact(11,20,35,2,12);
			System.out.println(t);
		}
		catch(TimeException ex){
			System.out.println(ex.getMessage());
		}

	}
}
