
public class Auto {
	private String kennzeichen;
	private double preis;
	
	public Auto(String kennzeichen, double preis) {
		setKennzeichen(kennzeichen);
		setPreis(preis);
	}

	public String getKennzeichen() {
		return kennzeichen;
	}

	public void setKennzeichen(String kennzeichen) {
		this.kennzeichen = kennzeichen;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}
	
	@Override
	public String toString() {
		return "Auto " + "(kennzeichen: " + kennzeichen + ", preis=" + preis + "�)";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Auto) {
			Auto at = (Auto) obj;
//			if (this.kennzeichen.equals(at.getKennzeichen()))
//				return true;
//			else 
//				return false;
			return this.kennzeichen.equals(at.getKennzeichen());
		}
		else 
			return false;
	}
}
