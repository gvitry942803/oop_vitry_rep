import java.util.Scanner;

public class Taschenrechner {
	static Scanner myScanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		
		System.out.println("Choose two number");
		float nb1 = myScanner.nextFloat();
		float nb2 = myScanner.nextFloat();
		
		System.out.println("Choose an operation between -,+,/,*");
		char op = myScanner.next().charAt(0);
		double res=0;
		switch(op) {
		case '+' : res = nb1 + nb2;
		break;
		case '-' : res = nb1 - nb2;
		break;
		case '*': res = nb1 * nb2;
		break;
		case '/': res = nb1 / nb2; 
		break;
		default : System.out.println("Wrong operation");
		}
		System.out.printf("%.1f %s %.1f = %.3f", nb1, op, nb2, res );
	}

}
