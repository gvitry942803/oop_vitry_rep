import java.util.Scanner;

public class Aufgabe2 {
	
	static int anzahl;
	static Scanner myScanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		anzahl = liesInt("Choose a number between 1 and 12");

		System.out.println(Monate(anzahl));
	}
	
	public static int liesInt(String chooseNumber) {
		System.out.println(chooseNumber);
		return myScanner.nextInt();
	}
	public static String Monate(int anzahl) {
		String m = "";
		switch (anzahl) {
			case 1: m = "Januar";
				break;
			case 2: m = "Februar";
				break;
			case 3: m = "Marz";
				break;
			case 4: m = "April";
				break;
			case 5: m = "Mai";
				break;
			case 6: m = "Juni";
				break;
			case 7: m = "Juli";
				break;
			case 8: m = "August";
				break;
			case 9: m = "September";
				break;
			case 10: m = "Oktober";
				break;
			case 11: m = "November";
				break;
			case 12: m = "Dezember";
				break;
			default: 
				System.out.println("Choose another number");
		}
		return(m);
	}
}
