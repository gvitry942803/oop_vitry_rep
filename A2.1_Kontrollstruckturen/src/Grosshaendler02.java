import java.util.Scanner;

public class Grosshaendler02 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("How many mouse want you order?");
		
		int nbM = myScanner.nextInt();
		
		int price = 30;
		int LP = 5;
		double fprice;
		
		if (nbM >= 10)
			fprice = price * nbM;
		else
			fprice = price * nbM + LP;
		
		if (fprice < 100) 
			fprice = fprice * 0.9;
		else {
			if (fprice > 100 && fprice < 500) 
				fprice = fprice * 0.85;
			else
				fprice = fprice * 0.80;
		}
		
		System.out.println("Order: "+nbM+" Mouses at " +price+ "� including TVA");
		System.out.println("Total Amount: "+fprice+"� including TVA");
	}

}
