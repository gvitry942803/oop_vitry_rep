package aufgabe2;

public class TagException extends Exception{
	private int ftag;
	
	public TagException(String msg, int ftag) {
		super(msg);
		this.ftag = ftag;
		
	}
	public int getfTag() {
		return ftag;
	}

}
