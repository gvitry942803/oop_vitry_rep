package Stack;

import java.util.Arrays;

public class Stack implements intStack{

	private int[] zliste;
	private int cp;
	
	public Stack() {
		zliste = new int[10];
	}
	
	public Stack(int laenge) {
		zliste = new int[laenge];
	}
	
	
	@Override
	public void push(int value) {
		if (cp < zliste.length) {
			zliste[cp] = value;
			cp++;
		}
	}

	@Override
	public int pop() {
		cp--;
		return zliste[cp];
	}

	public String toString() {
		String ergStr = "[";
		for (int i = 0; i < cp; i++)
			ergStr = ergStr + " " + zliste[i];
		ergStr = ergStr + " ]";
		return ergStr;
	}

}
