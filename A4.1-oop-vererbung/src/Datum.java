public class Datum {

	private int tag;
	private int monat;
	private int jahr;
	
	public Datum(int tag, int monat, int jahr) {
		setTag(tag);
		setMonat(monat);
		setJahr(jahr);
	}
	
	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}

	public int getTag() {
		return tag;
	}
	public void setTag(int tag) {
		this.tag = tag;
	}
	public int getMonat() {
		return monat;
	}
	public void setMonat(int monat) {
		this.monat = monat;
	}
	public int getJahr() {
		return jahr;
	}
	public void setJahr(int jahr) {
		this.jahr = jahr;
	}
	
	@Override
	public String toString() {
		return "Datum ("+ tag + "." + monat + "." + jahr + ")";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Datum) {
			Datum date = (Datum) obj;
			if (this.tag == date.getTag() && this.monat == date.getMonat() 
					&& this.jahr==date.getJahr())
				return true;
			else 
				return false;
		}
		else
			return false;
			 
	}
}
