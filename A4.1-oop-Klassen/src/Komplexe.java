
public class Komplexe {

	private double real;
	private double im;
	
	public Komplexe(double real, double im) {	
		setReal(real);
		setIm(im);
	}
	
	public Komplexe() {
		this.real = 0.0;
		this.im = 0.0;
	}

	public double getReal() {
		return real;
	}

	public void setReal(double real) {
		this.real = real;
	}

	public double getIm() {
		return im;
	}

	public void setIm(double im) {
		this.im = im;
	}

	@Override
	public String toString() {
		return "Komplexe (" + real + "+j" + im + ")";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Komplexe) {
			Komplexe k = (Komplexe) obj;
			return (this.real == k.getReal() && this.im == k.getIm());	
		}
		else 
			return false;
	}
	
	public static Komplexe multiplikation(Komplexe k1, Komplexe k2) {
		double re;
		double im;
		re = k1.getReal()*k2.getReal()-k1.getIm()*k2.getIm();
		im = k1.getReal()*k2.getIm()+k2.getReal()*k1.getIm();
		return (new Komplexe(re, im));	
	}
	
	public Komplexe multiplikation3(Komplexe k2) {
		double re;
		double im;
		re = this.getReal()*k2.getReal()-this.getIm()*k2.getIm();
		im = this.getReal()*k2.getIm()+k2.getReal()*this.getIm();
		return (new Komplexe(re, im));	
	}
	
	public Komplexe multiplication2(Object obj) {
		double real2 = 0;
		double im2=0;
		if (obj instanceof Komplexe) {
			Komplexe k2 = (Komplexe) obj;
			real2 = this.real*k2.getReal()-this.im*k2.getIm();
			im2 = this.real*k2.getIm()+k2.getReal()*this.im;
			return (new Komplexe(real2,im2));
		}
		else 
			return new Komplexe(real2,im2);
	}
	
	
}
