package GeoObject;


public class Rechteck extends GeoObject{
	private int hohe;
	private int breite;
	
	public Rechteck(double x, double y, int hohe, int breite) {
		super(x, y);
		this.hohe = hohe;
		this.breite = breite;
	}

	@Override
	public double determineArea() {
		return hohe * breite;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", " + hohe + ", " + breite;
	}
	
	
	
}
