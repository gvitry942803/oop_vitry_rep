
public class Wetterstatistik {
	public
		double mittelwert;
		double minimum;
		double maximum;
		
		
		public double getMittelwert(double[] temperatur) {
			double sum = 0;
			for(int i = 0; i < temperatur.length; i++) {
				sum = sum + temperatur[i];
				mittelwert = sum/temperatur.length;
			}
			return mittelwert;
		}


		public double getMinimum(double[] temperatur) {
			double minimum = temperatur[0];
			for(int i = 0; i < temperatur.length; i++) {
				if (temperatur[i] < minimum) {
		            minimum = temperatur[i]; 
		        }
			}
			return minimum;
		}


		public double getMaximum(double[] temperatur) {
			double maximum = temperatur[0];
			for(int i = 0; i < temperatur.length; i++) {
				if (temperatur[i] > maximum) {
		            maximum = temperatur[i]; 
		        }
			}
			return maximum;
		}
		

		
		
		
}
