
public class einDArrayBeispiel {

	public static void main(String[] args) {
		// Array Beispliele
		int [] zahlen = new int[] {1,2,3,4,5,6};
		int [] zahlen2 = { 9, 8, 7, 6, 5, 4, 3};
		
		int [] zahlen3 = new int[6];
		
		for( int i = 0; i<zahlen3.length; i++) {
			zahlen3[i] = i*2;
		}
		
//		for( int i = 0; i<zahlen3.length; i++) {
//			System.out.print(zahlen3[i] + " ");
//		}
		
//		for(int v:zahlen){
//			System.out.print(v + " ");
//		}
		
		
		int erg = sucheZahl(zahlen2, 6);
		System.out.println(erg); // Don't put the method in the print , just init a variable 
		 
	}
	
	//print the number at wert position in the array
	public static int sucheZahl(int [] zliste, int wert) {
		for (int i = 0; i < zliste.length; i++) {
			if (zliste[i] == wert)
				return i;
		}
		return -1;
	}

	public static void printArray(int [] zliste) {
		System.out.print("[ ");
		for (int i=0; i<zliste.length; i++) {
			System.out.print(zliste[i] + " ");
		}
		System.out.println("]");
	}
}
