
public class StudentTest {

	public static void main(String[] args) {
		Student s1;
		s1 = new Student("s123456");
		s1.setMNr("s9999");
		
		Student s2 = new Student(new String("s9999"));
		
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s1.equals(s2));
	}

}
