package aufgabe2;


public class Datum {
	private int tag;
	private int monat;
	private int jahr;
	
	
	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}

	public Datum(int tag, int monat, int jahr) {
//		setTag(tag);
//		setMonat(monat);
//		setJahr(jahr);
		
		this.tag = tag;
		this.monat = monat;
		this.jahr = jahr;
	}

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) throws TagException {
		if(tag>1 && tag <31)
			this.tag = tag;
		else {
//			throw new DatumException("Falschen Tag", tag, this.monat, this.jahr);
			throw new TagException("Falschen monat",monat);
		}
	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) throws MonatException {
		if(monat > 1 && monat < 12)
			this.monat = monat;
		else {
//			throw new DatumException("Falschen monat", tag, monat, jahr);
			throw new MonatException("Falschen monat",monat);
		}
	}

	public int getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) throws JahrException{
		if(jahr > 1900 && jahr < 2100) 
			this.jahr = jahr;
		else {
//			throw new DatumException("Falschen jahr", tag, monat, jahr);
			throw new JahrException("Falschen monat",monat);
		}
	}

	public static int berechneQuartal(Datum d){
		return d.monat/3 +1;
	}
	
	@Override
	public boolean equals(Object obj) {
		Datum d = (Datum) obj;
		if (this.tag == d.tag && this.monat == d.monat && this.jahr == d.jahr)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return this.tag+"."+this.monat+"."+this.jahr;
	}
		

}