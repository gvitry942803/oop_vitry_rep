package aufgabe2;

public class MonatException extends Exception{
	private int fmonat;
	
	public MonatException(String msg, int fmonat) {
		super(msg);
		this.fmonat = fmonat;
		
	}
	public int getfMonat() {
		return fmonat;
	}

}
