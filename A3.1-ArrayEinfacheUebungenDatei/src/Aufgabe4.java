
public class Aufgabe4 {
	public static void main(String[] args) {
		int[] loto = new int[] {3, 7, 12, 18, 37, 42};
		printTable(loto);
		verifTable(loto);
	}
	public static void printTable(int[] table) {
		System.out.print("[ ");
		for(int i=0; i < table.length; i++) {
			System.out.printf("%d ", table[i]);
		}
		System.out.print("]\n");
	}
	
	public static void verifTable(int[] table) {
		String r12 = "";
		String r13 = "";
		for(int i=0; i < table.length; i++) {
			if(table[i] == 12)
				r12 = "Die Zahl 12 ist in der Ziehung enthalten";
			
			if(table[i] == 13)
				r13 = "Die Zahl 13 ist in der Ziehung enthalten";
			else
				r13 = "Die Zahl 13 ist nicht in der Ziehung enthalten.";
		}
		System.out.println(r12);
		System.out.println(r13);
	}
	
}
