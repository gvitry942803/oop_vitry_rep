
public class Person {

	private String name;
	private Datum Geburtsdatum;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Datum getGeburtsdatum() {
		return Geburtsdatum;
	}
	public void setGeburtsdatum(Datum geburtsdatum) {
		Geburtsdatum = geburtsdatum;
	}
	public Person(String name, Datum geburtsdatum) {
		setName(name);
		setGeburtsdatum(geburtsdatum);
	}
	
	
	
}
